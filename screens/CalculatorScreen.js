require("../lib/swisscalc.lib.format.js");
require("../lib/swisscalc.lib.operator.js");
require("../lib/swisscalc.lib.operatorCache.js");
require("../lib/swisscalc.lib.shuntingYard.js");
require("../lib/swisscalc.display.numericDisplay.js");
require("../lib/swisscalc.display.memoryDisplay.js");
require("../lib/swisscalc.calc.calculator.js");

import React from 'react';
import { StyleSheet, Dimensions, PanResponder, View, SafeAreaView, Text } from 'react-native';
import { CalcDisplay, CalcButton } from './../components';

export default class CalculatorScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      display: "0",
      orientation: "portrait",    // "portrait" or "landscape"
    }

    // Initialize calculator...
    this.oc = global.swisscalc.lib.operatorCache;
    this.calc = new global.swisscalc.calc.calculator();

    // Listen for orientation changes...
    Dimensions.addEventListener('change', () => {
      const { width, height } = Dimensions.get("window");
      var orientation = (width > height) ? "landscape" : "portrait";
      this.setState({ orientation: orientation });
    });

    // Setup gestures...
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetPanResponder: (evt, gestureState) => true,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
      onPanResponderMove: (evt, gestureState) => { },
      onPanResponderRelease: (evt, gestureState) => {
        if (Math.abs(gestureState.dx) >= 50) {
          this.onBackspacePress();
        }
      },
    })
  }

  onDigitPress = (digit) => {
    this.calc.addDigit(digit);
    this.setState({ display: this.calc.getMainDisplay() });
  }

  onUnaryOperatorPress = (operator) => {
    this.calc.addUnaryOperator(operator);
    this.setState({ display: this.calc.getMainDisplay() });
  }

  onBinaryOperatorPress = (operator) => {
    this.calc.addBinaryOperator(operator);
    this.setState({ display: this.calc.getMainDisplay() });
  }

  onEqualsPress = () => {
    this.calc.equalsPressed();
    this.setState({ display: this.calc.getMainDisplay() });
  }

  onClearPress = () => {
    this.calc.clear();
    this.setState({ display: this.calc.getMainDisplay() });
  }

  onPlusMinusPress = () => {
    this.calc.negate();
    this.setState({ display: this.calc.getMainDisplay() });
  }

  onPercentPress = () => {
    let digit = parseFloat(this.calc.getMainDisplay())
    this.calc.clear();
    this.calc.addDigit(digit / 100)
    this.setState({ display: this.calc.getMainDisplay() });
  }

  onBackspacePress = () => {
    this.calc.backspace();
    this.setState({ display: this.calc.getMainDisplay() });
  }

  renderPortrait() {
    return (
      <View style={styles.container}>
        <View style={{ flex: 3, justifyContent: "flex-end" }} {...this.panResponder.panHandlers}>
          <CalcDisplay display={this.state.display} />
        </View>

        <View style={{ flexDirection: "row", flex: 1 }}>
          <CalcButton style={{ backgroundColor: "#646466" }} onPress={this.onClearPress} title="AC" color="white" backgroundColor="#DCC894" />
          <CalcButton style={{ flex: 2, backgroundColor: "#646466" }} />
          <CalcButton style={{ backgroundColor: "#EEA639" }} onPress={() => { this.onBinaryOperatorPress(this.oc.DivisionOperator) }} title="/" color="white" backgroundColor="#DCA394" />
        </View>

        <View style={{ flexDirection: "row", flex: 1 }}>
          <CalcButton onPress={() => { this.onDigitPress("7") }} title="7" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("8") }} title="8" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("9") }} title="9" color="white" backgroundColor="#607D8B" />
          <CalcButton style={{ backgroundColor: "#EEA639" }} onPress={() => { this.onBinaryOperatorPress(this.oc.MultiplicationOperator) }} title="x" color="white" backgroundColor="#DCA394" />
        </View>

        <View style={{ flexDirection: "row", flex: 1 }}>
          <CalcButton onPress={() => { this.onDigitPress("4") }} title="4" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("5") }} title="5" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("6") }} title="6" color="white" backgroundColor="#607D8B" />
          <CalcButton style={{ backgroundColor: "#EEA639" }} onPress={() => { this.onBinaryOperatorPress(this.oc.SubtractionOperator) }} title="-" color="white" backgroundColor="#DCA394" />
        </View>

        <View style={{ flexDirection: "row", flex: 1 }}>
          <CalcButton onPress={() => { this.onDigitPress("1") }} title="1" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("2") }} title="2" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("3") }} title="3" color="white" backgroundColor="#607D8B" />
          <CalcButton style={{ backgroundColor: "#EEA639" }} onPress={() => { this.onBinaryOperatorPress(this.oc.AdditionOperator) }} title="+" color="white" backgroundColor="#DCA394" />
        </View>

        <View style={{ flexDirection: "row", flex: 1 }}>
          <CalcButton onPress={() => { this.onDigitPress("0") }} title="0" color="white" backgroundColor="#607D8B" style={{ flex: 2 }} />
          <CalcButton style={{ flex: 1 }} onPress={() => { this.onDigitPress(".") }} title="." color="white" backgroundColor="#607D8B" />
          <CalcButton style={{ backgroundColor: "#EEA639" }} onPress={this.onEqualsPress} title="=" color="white" />
        </View>
      </View>
    );
  }

  renderLandscape() {
    return (
      <View style={horizontalStyles.container}>
        <View style={{ flex: 2, justifyContent: "flex-start" }} {...this.panResponder.panHandlers}>
          <CalcDisplay display={this.state.display} style={{ backgroundColor: "white"}} />
        </View>

        <View style={{ flexDirection: "row", flex: 1 }}>
          <CalcButton style={{ backgroundColor: "#646466" }} onPress={() => { this.onUnaryOperatorPress(this.oc.LogBase10Operator) }} title="Log10" color="white" backgroundColor="#607D8B" />
          <CalcButton style={{ backgroundColor: "#646466" }} onPress={this.onClearPress} title="AC" color="white" backgroundColor="#DCC894" />
          <CalcButton style={{ backgroundColor: "#646466" }} onPress={this.onPlusMinusPress} title="+/-" color="white" backgroundColor="#DCC894" />
          <CalcButton style={{ backgroundColor: "#646466" }} onPress={this.onPercentPress} title="%" color="white" backgroundColor="#DCC894" />
          <CalcButton style={{ backgroundColor: "#EEA639" }} onPress={() => { this.onBinaryOperatorPress(this.oc.DivisionOperator) }} title="/" color="white" backgroundColor="#DCA394" />
        </View>

        <View style={{ flexDirection: "row", flex: 1 }}>
          <CalcButton style={{ backgroundColor: "#646466" }} onPress={() => { this.onUnaryOperatorPress(this.oc.NaturalLogOperator) }} title="LogN" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("7") }} title="7" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("8") }} title="8" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("9") }} title="9" color="white" backgroundColor="#607D8B" />
          <CalcButton style={{ backgroundColor: "#EEA639" }} onPress={() => { this.onBinaryOperatorPress(this.oc.MultiplicationOperator) }} title="x" color="white" backgroundColor="#DCA394" />
        </View>

        <View style={{ flexDirection: "row", flex: 1 }}>
          <CalcButton style={{ backgroundColor: "#646466" }} onPress={() => { this.onUnaryOperatorPress(this.oc.XSquaredOperator) }} title="x2" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("4") }} title="4" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("5") }} title="5" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("6") }} title="6" color="white" backgroundColor="#607D8B" />
          <CalcButton style={{ backgroundColor: "#EEA639" }} onPress={() => { this.onBinaryOperatorPress(this.oc.SubtractionOperator) }} title="-" color="white" backgroundColor="#DCA394" />
        </View>

        <View style={{ flexDirection: "row", flex: 1 }}>
          <CalcButton style={{ backgroundColor: "#646466" }} onPress={() => { this.onUnaryOperatorPress(this.oc.XCubedOperator) }} title="x3" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("1") }} title="1" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("2") }} title="2" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("3") }} title="3" color="white" backgroundColor="#607D8B" />
          <CalcButton style={{ backgroundColor: "#EEA639" }} onPress={() => { this.onBinaryOperatorPress(this.oc.AdditionOperator) }} title="+" color="white" backgroundColor="#DCA394" />
        </View>

        <View style={{ flexDirection: "row", flex: 1 }}>
          <CalcButton style={{ backgroundColor: "#646466" }} onPress={() => { this.onUnaryOperatorPress(this.oc.EExponentialOperator) }} title="e" color="white" backgroundColor="#607D8B" />
          <CalcButton onPress={() => { this.onDigitPress("0") }} title="0" color="white" backgroundColor="#607D8B" style={{ flex: 2 }} />
          <CalcButton style={{ flex: 1 }} onPress={() => { this.onDigitPress(".") }} title="." color="white" backgroundColor="#607D8B" />
          <CalcButton style={{ backgroundColor: "#EEA639" }} onPress={this.onEqualsPress} title="=" color="white" />
        </View>
      </View>
    );
  }

  render() {
    var view = (this.state.orientation == "portrait")
      ? this.renderPortrait()
      : this.renderLandscape();

    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "#545557"}}>
        <View style={{ flex: 1 }}>
          {view}
        </View>
      </SafeAreaView>
    )
  }

}

const styles = StyleSheet.create({
  container: { flex: 1, paddingVertical: 50, backgroundColor: "#545557" },
})

const horizontalStyles = StyleSheet.create({
  container: { flex: 1, backgroundColor: "#545557"},
})