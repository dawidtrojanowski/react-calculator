
import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';

export default class CalcButton extends React.Component {

  static defaultProps = {
    onPress: function() { },
    title: "",
    color: "white",
    backgroundColor: "black",

    style: { },
  }

  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} activeOpacity={0.5}
        style={[styles.container, { flex: 1, backgroundColor: '#807E7D' }, { ...this.props.style }]}>
        <Text style={[styles.text, { color: this.props.color }]}>{this.props.title}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: { alignItems: "center", justifyContent: "center", margin: 0.6 },
  text: { fontSize: 30 },
});